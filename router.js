const httpConstants = require('./src/constants/http');

module.exports = async (request, response, routes) => {
  const route = routes.find((route) => {
    const httpMethod = route.method === request.method;
    let existsPath = false;

    if (typeof route.path === 'object') {
      existsPath = request.url.match(route.path);
    } else {
      existsPath = route.path === request.url;
    }
    return httpMethod && existsPath;
  });

  let param = null;
  let param2 = null;

  // GET id param from URL
  if (route && typeof route.path === 'object') {
    param = request.url.match(route.path)[1];
    param2 = request.url.match(route.path)[2];
  }

  if (route) {
    let body = null;
    if (request.method === httpConstants.HTTP_METHOD_POST || request.method === httpConstants.HTTP_METHOD_PUT) {
      body = await getRequestData(request);
    }
    return route.handler(request, response, param, param2, body);
  }
  response.statusCode = httpConstants.HTTP_STATUS_NOT_FOUND;
  response.setHeader(httpConstants.HTTP_HEADER_CONTENT_TYPE, httpConstants.HTTP_MEDIA_TYPE_APPLICATION_JSON);
  return response.end();
};

function getRequestData(request) {
  return new Promise(((resolve, reject) => {
    try {
      let body = [];
      request.on('error', (err) => {
        console.error(err);
      }).on('data', (value) => {
        body.push(value);
      }).on('end', () => {
        let json = JSON.parse(Buffer.concat(body).toString());
        resolve(json);
      });
    } catch (e) {
      reject(e);
    }
  }));
}

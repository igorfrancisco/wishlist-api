const httpConstants = require('./src/constants/http');
const userController = require('./src/controller/UserController');
const wishListController = require('./src/controller/WishListController');

const routes = [
  {
    method: httpConstants.HTTP_METHOD_GET,
    path: /\/user\/([0-9a-z]+)\/(\B)/,
    handler: userController.findById.bind(userController)
  },
  {
    method: httpConstants.HTTP_METHOD_PUT,
    path: /\/user\/([0-9a-z]+)/,
    handler: userController.updateById.bind(userController)
  },
  {
    method: httpConstants.HTTP_METHOD_DELETE,
    path: /\/user\/([0-9a-z]+)\/(\B)/,
    handler: userController.deleteById.bind(userController)
  },
  {
    method: httpConstants.HTTP_METHOD_POST,
    path: '/user',
    handler: userController.createUser.bind(userController)
  },
  {
    method: httpConstants.HTTP_METHOD_GET,
    path: '/user',
    handler: userController.findAll.bind(userController)
  },
  {
    method: httpConstants.HTTP_METHOD_POST,
    path: /\/user\/([0-9a-z]+)\/wishlist/,
    handler: wishListController.addProductToWishList.bind(wishListController)
  },
  {
    method: httpConstants.HTTP_METHOD_GET,
    path: /\/user\/([0-9a-z]+)\/wishlist/,
    handler: wishListController.showWishList.bind(wishListController)
  },
  {
    method: httpConstants.HTTP_METHOD_DELETE,
    path: /\/user\/([0-9a-z]+)\/wishlist\/product\/([0-9a-z\W]+)/,
    handler: wishListController.deleteProductFromWishList.bind(wishListController)
  }
];

module.exports = routes;

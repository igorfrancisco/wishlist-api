FROM node:8.16.1

WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm install

COPY . /usr/src/app
EXPOSE 3000

ENV TZ 'America/Sao_Paulo'

CMD ["node","index.js"]

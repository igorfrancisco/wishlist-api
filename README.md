# WishList API

### Dependências
*  Node 8.16+
*  Docker
*  docker-compose
  
### Executando a aplicação local utilizando docker-compose

```shell script
$ docker-compose -f docker-compose-local.yml up --build
``` 

- Após a execução do docker-compose, a aplicação responderá em http://localhost:3000

- Enpoints disponíveis:

* **GET** /user/{:userId}
* **PUT** /user/{:userId}/
* **DELETE** /user/{:userId}
* **POST** /user
* **GET** /user
* **POST** /user/{:userId}/wishlist/
* **GET** /user/{:userId}/wishlist/
* **DELETE** /user/{:userId}/wishlist\/product/{:productId}

-  Criação de um usuário:
```shell script
curl --request POST \
  --url http://localhost:3000/user \
  --header 'content-type: application/json' \
  --data '{
    {
        "name":"Usuário1"
        "email":"usuario1@email.com"
    }
  }'
```


### Checklist
* [x]  Criar Cliente
* [x]  Atualizar Cliente
* [x]  Visualizar Cliente
* [x]  Remover Cliente
* [ ]  Autenticação e Autorização
* [ ]  Testes Unitários
* [ ]  Testes de integração
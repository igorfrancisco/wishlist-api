const http = require('http');

require('dotenv/config');
const {APPLICATION_HOSTNAME, APPLICATION_PORT} = require('./src/constants/default');
const routes = require('./routes');
const router = require('./router');

process.on('uncaughtException', (err) => {
  console.error(err.stack);
  console.log(err);
});

const server = http.createServer(async (request, response) => {
  await router(request, response, routes);
});

server.listen(APPLICATION_PORT, APPLICATION_HOSTNAME, () => {
  console.log(`Server running at http://${APPLICATION_HOSTNAME}:${APPLICATION_PORT}/`);
});

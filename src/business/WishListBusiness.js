const productAPI = require('../service/ProductAPI');
const userBusiness = require('../business/UserBusiness');
let WishListModel = require('../model/WishListModel');

class WishListBusiness {
  async addProductToWishList(userId, productList) {
    const user = await userBusiness.findById(userId);
    if (!user) {
      throw new Error(`user with id: ${userId} not found.`);
    }
    let validProducts = [];
    for (let i = 0; i < productList.length; i++) {
      try {
        let productFromCatalog = await productAPI.findProductById(productList[i]);
        if (productFromCatalog && productFromCatalog.id) {
          validProducts.push(productFromCatalog.id);
        }
      } catch (error) {
        console.log(`There was an error on search product ${error}`)
      }
    }
    let query = {userId: userId},
      options = {upsert: true, new: true, setDefaultsOnInsert: true};
    WishListModel.findOneAndUpdate(query, options, (error, document) => {
      if (document == null) {
        document = new WishListModel();
        document.userId = userId;
      }
      document.products = [...new Set(validProducts)];
      document.save();
    });
  }

  async removeProductFromWishList(userId, productId) {
    const user = await userBusiness.findById(userId);
    if (!user) {
      throw new Error(`user with id: ${userId} not found.`);
    }
    let query = {userId: userId},
      options = {upsert: true, new: true, setDefaultsOnInsert: true};
    WishListModel.findOneAndUpdate(query, options, (error, document) => {
      if (error) throw new Error(error);
      document.products = document.products.filter(element => element !== productId);
      document.save();
    });
  }

  async showWishList(userId) {
    const user = await userBusiness.findById(userId);
    if (!user) {
      throw new Error(`user with id: ${userId} not found.`);
    }
    let response = user;
    response.products = [];
    let wishList = await WishListModel.findOne({userId: userId});
    let products = wishList.products;
    for (let i = 0; i < products.length; i++) {
      try {
        let productFromCatalog = await productAPI.findProductById(products[i]);
        response.products.push(productFromCatalog);
      } catch (error) {
        console.log(`There was an error on search product. Error: ${error}`)
      }
    }
    return response;
  }
}

module.exports = new WishListBusiness();

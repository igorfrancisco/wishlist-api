let UserModel = require('../model/UserModel');

class UserBusiness {
  async findById(id) {
    return UserModel.findById(id).lean();
  }

  async createUser(user) {
    return UserModel(user).save();
  }

  async updateById(id, user) {
    user._id = id;
    return this.createUser(user);
  }

  async deleteById(id) {
    return UserModel.deleteOne({ _id : id});
  }

  async findAll() {
    return UserModel.find();
  }
}

module.exports = new UserBusiness();

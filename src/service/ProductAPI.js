const http = require('http');
const {PRODUCT_API_HOST} = require('../constants/default');

class ProductAPI {
  async findProductById(id) {
    return new Promise((resolve, reject) => {
      try {
        http.get(PRODUCT_API_HOST + id, (response) => {
          let data = '';
          response.on('data', (chunk) => {
            data += chunk;
          });

          response.on('end', () => {
            resolve(JSON.parse(data));
          });
        }).on("error", (err) => {
          console.log("Error: " + err.message);
        });
      } catch (error) {

      }
    });
  }
}

module.exports = new ProductAPI();

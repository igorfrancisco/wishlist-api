const UserBusiness = require('../business/UserBusiness');
const httpConstants = require('../constants/http');

class UserController {
  async findById(request, response, param) {
    const user = await UserBusiness.findById(param);
    let statusCode = httpConstants.HTTP_STATUS_OK;
    if (!user) {
      statusCode = httpConstants.HTTP_STATUS_NOT_FOUND;
    }
    response.statusCode = statusCode;
    response.setHeader(httpConstants.HTTP_HEADER_CONTENT_TYPE, httpConstants.HTTP_MEDIA_TYPE_APPLICATION_JSON);
    response.end(JSON.stringify(user));
  }

  async createUser(request, response, param, param2, data) {
    response.setHeader(httpConstants.HTTP_HEADER_CONTENT_TYPE, httpConstants.HTTP_MEDIA_TYPE_APPLICATION_JSON);
    try {
      await UserBusiness.createUser(data);
      response.statusCode = httpConstants.HTTP_STATUS_CREATED;
      response.end();
    } catch (error) {
      response.statusCode = httpConstants.HTTP_STATUS_INTERNAL_SERVER_ERROR;
      console.log(error);
    }
    response.end();
  }

  async updateById(request, response, param, param2, data) {
    response.setHeader(httpConstants.HTTP_HEADER_CONTENT_TYPE, httpConstants.HTTP_MEDIA_TYPE_APPLICATION_JSON);
    try {
      response.statusCode = httpConstants.HTTP_STATUS_NO_CONTENT;
      await UserBusiness.updateById(param, data);
      response.end();
    } catch (e) {
      if (e instanceof mongoose.DocumentNotFoundError) {
        response.statusCode = httpConstants.HTTP_STATUS_NOT_FOUND;
      }
    }
    response.end();
  }

  async deleteById(request, response, param) {
    response.setHeader(httpConstants.HTTP_HEADER_CONTENT_TYPE, httpConstants.HTTP_MEDIA_TYPE_APPLICATION_JSON);
    await UserBusiness.deleteById(param);
    response.statusCode = httpConstants.HTTP_STATUS_NO_CONTENT;
    response.end();
  }

  async findAll(request, response) {
    const users = await UserBusiness.findAll() || [];
    response.statusCode = httpConstants.HTTP_STATUS_OK;
    response.setHeader(httpConstants.HTTP_HEADER_CONTENT_TYPE, httpConstants.HTTP_MEDIA_TYPE_APPLICATION_JSON);
    response.end(JSON.stringify(users));
  }
}

module.exports = new UserController();

const WishListBusiness = require('../business/WishListBusiness');
const httpConstants = require('../constants/http');

class WishListController {
  async addProductToWishList(request, response, userId, param2, data) {
    response.setHeader(httpConstants.HTTP_HEADER_CONTENT_TYPE, httpConstants.HTTP_MEDIA_TYPE_APPLICATION_JSON);
    try {
      await WishListBusiness.addProductToWishList(userId, data);
      response.statusCode = httpConstants.HTTP_STATUS_CREATED;
      response.end();
    } catch (error) {
      response.statusCode = httpConstants.HTTP_STATUS_INTERNAL_SERVER_ERROR;
      console.log(error);
    }
    response.end();
  }

  async deleteProductFromWishList(request, response, userId, productId) {
    response.setHeader(httpConstants.HTTP_HEADER_CONTENT_TYPE, httpConstants.HTTP_MEDIA_TYPE_APPLICATION_JSON);
    try {
      await WishListBusiness.removeProductFromWishList(userId, productId);
      response.statusCode = httpConstants.HTTP_STATUS_NO_CONTENT;
    } catch (error) {
      response.statusCode = httpConstants.HTTP_STATUS_INTERNAL_SERVER_ERROR;
      console.log(error);
    }
    response.end();
  }

  async showWishList(request, response, userId) {
    response.setHeader(httpConstants.HTTP_HEADER_CONTENT_TYPE, httpConstants.HTTP_MEDIA_TYPE_APPLICATION_JSON);
    try {
      const wishList = await WishListBusiness.showWishList(userId);
      response.statusCode = httpConstants.HTTP_STATUS_OK;
      response.end(JSON.stringify(wishList));
    } catch (error) {
      response.statusCode = httpConstants.HTTP_STATUS_INTERNAL_SERVER_ERROR;
      console.log(error);
    }
    response.end();
  }
}

module.exports = new WishListController();

const database = require('../config/database');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');

let UserSchema = new Schema({
  name: String,
  email: {
    type: String,
    unique: true,
    lowercase: true,
    required: [true, 'Required field.'],
    validate: {
      validator: (v) => {
        return /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(v);
      },
      message: '{VALUE} is not a valid email!'
    },
    index: true
  },
}, {timestamps: true});
UserSchema.plugin(uniqueValidator, {message: 'Already exists.'});

module.exports = database.model('User', UserSchema);

const database = require('../config/database');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let WishListSchema = new Schema({
  userId: {type: Schema.Types.ObjectId, ref: 'User', unique: true},
  products: [{type: String}]
}, {timestamps: true});

module.exports = database.model('WishList', WishListSchema);

const {DATABASE_HOST, DATABASE_PORT} = require('../constants/default');
const mongoose = require('mongoose');

const connectionInstance = mongoose.createConnection(`mongodb://${DATABASE_HOST}:${DATABASE_PORT}/wishlist`, {useNewUrlParser: true});
connectionInstance.on('error', (err) => {
  if (err) {
    throw err;
  }
});

connectionInstance.once('open', () => {
  console.log('MongoDb connected successfully');
});

module.exports = connectionInstance;
